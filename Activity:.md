Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


Business Logic in Creating a user "/signup"
1. Add a functionality to check if there are duplicates tasks
        - if the user already exists, return error Or "Already registered"
        - if the user does not exist, we add it on our database
                1. The user data will be coming from the req.body
                2. Create a new User object with a "username" and "password" fields/properties
                3. Then save, and add an error handling