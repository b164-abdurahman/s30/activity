const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 5000;


mongoose.connect('mongodb+srv://hiccupdusk:hiccupdusk@cluster1.6vk35.mongodb.net/b164_todo?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true    
		}
)
let db = mongoose.connection;

//connection error message
db.on("error", console.error.bind(console, "connection error"));
//connection successful message
db.once("open", () => console.log("We're connected to the cloud database"))

app.use(express.json());

app.use(express.urlencoded({ extended:true }));

app.listen(port, ()=> console.log(`Server running at port ${port}`));

const taskSchema = new mongoose.Schema({
	username: String,
    password: String,
	status: {
		type: String,
		//Default values are the predefined values for a field if we don't put any value.
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

app.post("/signup", (req, res) => {
	Task.findOne({ username: req.body.name }, (err, result) => {
		
		if(result != null && result.username == req.body.name && result.password == req.body.password){
			return res.send("User Already Registered");
		} else{

			let newTask = new Task({
				username: req.body.username,
                password: req.body.password
			});

			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New task created")
				}
			})


		}
	})
})

app.get("/signup", (req, res) => {
    Task.find({}, (err, result) => {
        if(err){
            return console.log(err);
        } else {
            return res.status(200).json({
                dataFromMGD: result
            })
        }
    })
})